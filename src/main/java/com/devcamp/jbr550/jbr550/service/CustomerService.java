package com.devcamp.jbr550.jbr550.service;

import java.util.ArrayList;

import com.devcamp.jbr550.jbr550.model.Customer;

public class CustomerService {
    Customer customer1 = new Customer(1, "QuanNM", 10);
    Customer customer2 = new Customer(2, "BoiHB", 15);
    Customer customer3 = new Customer(3, "ThuNHM", 12);

    public ArrayList<Customer> getListCustomer() {
        ArrayList<Customer> listCustomer = new ArrayList<>();
        listCustomer.add(customer1);
        listCustomer.add(customer2);
        listCustomer.add(customer3);
        return listCustomer;
    }
}

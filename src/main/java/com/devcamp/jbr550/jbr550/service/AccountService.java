package com.devcamp.jbr550.jbr550.service;

import java.util.ArrayList;

import com.devcamp.jbr550.jbr550.model.Account;

public class AccountService {
    Account account1 = new Account(1, new CustomerService().customer1);
    Account account2 = new Account(2, new CustomerService().customer2, 700);
    Account account3 = new Account(3, new CustomerService().customer3, 300);

    public ArrayList<Account> getListAccount() {
        ArrayList<Account> listAccount = new ArrayList<>();
        listAccount.add(account1);
        listAccount.add(account2);
        listAccount.add(account3);
        return listAccount;
    }
}

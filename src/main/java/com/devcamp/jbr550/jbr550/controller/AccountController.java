package com.devcamp.jbr550.jbr550.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr550.jbr550.model.Account;
import com.devcamp.jbr550.jbr550.service.AccountService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class AccountController {
    @GetMapping("/accounts")
    public ArrayList<Account> getAllAccount() {
        return new AccountService().getListAccount();
    }
}

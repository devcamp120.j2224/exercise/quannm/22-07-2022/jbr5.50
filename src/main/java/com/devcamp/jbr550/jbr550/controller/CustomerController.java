package com.devcamp.jbr550.jbr550.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr550.jbr550.model.Customer;
import com.devcamp.jbr550.jbr550.service.CustomerService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CustomerController {
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomer() {
        return new CustomerService().getListCustomer();
    }
}
